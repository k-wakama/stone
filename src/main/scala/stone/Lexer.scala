package stone

import java.io.{IOException, LineNumberReader, Reader}
import java.util.regex.{Matcher, Pattern}

import scala.collection.mutable.ListBuffer

/**
 * 字句解析器
 */
class Lexer(r: Reader) {
  import Lexer._

  /**
   * 次の行があるかどうか
   */
  var hasMore: Boolean = true
  val reader: LineNumberReader = new LineNumberReader(r)
  val queue: ListBuffer[Token] = ListBuffer.empty

  def read(): Token = {
    if (fillQueue(0)) {
      queue.remove(0)
    } else {
      Token.EOF
    }
  }

  def peek(i: Int): Token = {
    if (fillQueue(i)) {
      queue(i)
    } else {
      Token.EOF
    }
  }

  /**
   * キューに積まれたトークンの数が指定された数以上になるようにトークンをキューに積む
   * @param i
   * @return
   */
  private def fillQueue(i: Int): Boolean = {
    while (i >= queue.size) {
      if (hasMore) {
        readLine()
      } else {
        return false
      }
    }
    return true
  }

  /**
   * 行からトークンを取り出してキューに詰める
   */
  protected def readLine(): Unit = {
    val line = try {
      reader.readLine()
    } catch {
      case e: IOException => throw new ParseException(e)
    }
    if (line == null) {
      hasMore = false
    } else {
      val lineNo = reader.getLineNumber
      val matcher: Matcher = pattern.matcher(line)
      matcher.useTransparentBounds(true).useAnchoringBounds(false)
      var pos = 0
      val endPos = line.length
      while (pos < endPos) { // 空行はここの条件に合わないのでスキップされる
        matcher.region(pos, endPos)
        if (matcher.lookingAt() /*&& matcher.end != matcher.start*/) {
          addToken(lineNo, matcher)
          pos = matcher.end
        } else {
          throw new ParseException(s"""bad token at line ${lineNo}. line = "${line}"""")
        }
      }
      queue += IdToken(Token.EOL, lineNo)
    }
  }

  protected def addToken(lineNo: Int, matcher: Matcher): Unit = {
    val m: String = matcher.group(1)
    if (m != null) {
      // スペースじゃない
      if (matcher.group(2) == null) {
        // コメントじゃない
        val token: Token =
          if (matcher.group(3) != null) {
            // 数値リテラルである
            NumToken(Integer.parseInt(m), lineNo)
          } else if (matcher.group(4) != null) {
            // 文字列リテラルである
            StrToken(toStringLiteral(m), lineNo)
          } else {
            // 識別子トークンである
            IdToken(m, lineNo)
          }
        queue += token
      }
    }
  }


  class ParseException(e: IOException) extends Exception(e) {

    def this(m: String) = {
      this(new IOException(m))
    }

    def this(m: String, t: Token) = {
      this(s"syntax error around ${t.location}. ${m}")
    }

    def this(t: Token) = {
      this("", t)
    }
  }
}

/**
 * [[Lexer]]
 */
object Lexer {
  /**
   * \s*((コメント)|(整数リテラル)|(文字列リテラル)|識別子)?
   * \s+|(\s*((コメント)|(整数リテラル)|(文字列リテラル)|識別子))
   */
  private val regexPat = """\s+|\s*((//.*)|([0-9]+)|("(\\"|\\\\|\\n|[^"])*")|[A-Z_a-z][A-Z_a-z0-9]*|==|<=|>=|&&|\|\||\p{Punct})"""
  private val pattern: Pattern = Pattern.compile(regexPat)

  private def toStringLiteral(s: String): String = {
    @annotation.tailrec
    def loop(s: String, acc: StringBuffer): StringBuffer = {
      if (s == "") {
        acc
      } else if (s.startsWith("\"")) {
        loop(s.drop(1), acc.append('"'))
      } else if (s.startsWith("\\\\")) {
        loop(s.drop(2), acc.append('\\'))
      } else if (s.startsWith("\\n")) {
        loop(s.drop(2), acc.append('\n'))
      } else {
        loop(s.drop(1), acc.append(s.head))
      }
    }
    loop(s, new StringBuffer()).toString
  }
}
