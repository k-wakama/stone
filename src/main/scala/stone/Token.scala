package stone

/**
 * 字句解析におけるトークン
 */
abstract class Token(private val lineNumber: Int) {
  def getLineNumber: Int = lineNumber
  def isIdentifier: Boolean = false
  def isNumber: Boolean = false
  def isString: Boolean = false
  def getNumber: Int = throw new StoneException("not number token")
  def getText: String = ""
  def location = if (this == Token.EOF) {"the last line"} else {s""""${this.getText}" at line ${this.getLineNumber}"""}
}

/**
 * 数値リテラルトークン
 * 
 * @param lineNumber
 * @param value
 */
case class NumToken(private val value: Int, lineNumber: Int) extends Token(lineNumber) {
  override def isNumber = true
  override def getText = value.toString
  override def getNumber = value
}

/**
 * 識別子トークン
 * @param text
 * @param lineNumber
 */
case class IdToken(private val text: String, lineNumber: Int) extends Token(lineNumber) {
  override def isIdentifier = true
  override def getText = text
}

/**
 * 文字列リテラルトークン
 * @param literal
 * @param lineNumber
 */
case class StrToken(private val literal: String, lineNumber: Int) extends Token(lineNumber) {
  override def isString = true
  override def getText = literal
}

/**
 * [[Token]]
 */
object Token {
  /**
   * ファイルの終わりを表すトークン
   */
  val EOF: Token = new Token(-1){}
  /**
   * 行の終わりを表すトークン
   */
  val EOL: String = "\\n"
}

class StoneException(m: String) extends RuntimeException(m)
