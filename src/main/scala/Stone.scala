import java.io.StringReader

import stone.{Token, Lexer}

/**
 * Created by wakamatsu on 15/06/13.
 */
object Stone {
  def main(args: Array[String]): Unit = {
    val l = new Lexer(new StringReader(
      """
        |
        |
        |sum = 0
        |while i < 10 {
        |  sum = sum + 1
        |  i = i + 1
        |}
        |
        |
        |"hoge" + "fuga"
        |"hoge" + あ
        |sum
        |hoge = "hoge\"fuga"
      """.stripMargin
    ))

    var t: Token = l.read
    while (t != Token.EOF) {
      println(s"=> ${t.getText}")
      t = l.read
    }
  }
}
